import { Injectable } from '@angular/core';

import { Customer } from '../lib/types';
import { CUSTOMERS } from '../resources/customer.data';

@Injectable()
export class CustomerService {
  getCustomers(): Promise<Customer[]> {
    return Promise.resolve(CUSTOMERS);
  };

  getHeroesSlowly(): Promise<Customer[]> {
    return new Promise(resolve => {
      setTimeout(() => resolve(this.getCustomers()), 2000);
    });
  };
}
