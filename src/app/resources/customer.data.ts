import { Customer } from '../lib/types';

export const CUSTOMERS: Customer[] = [
  { "id": 11, "name": "Weldon Poliks" },
  { "id": 12, "name": "Caterina Luncsford" },
  { "id": 13, "name": "Pansy Schrameck" },
  { "id": 14, "name": "Elli Dokka" },
  { "id": 15, "name": "Hilde Baucom" },
  { "id": 16, "name": "Vernell Seemann" },
  { "id": 17, "name": "Dyan Mynear" },
  { "id": 18, "name": "Niesha Glovinsky" },
  { "id": 19, "name": "Laquita Deatley" },
  { "id": 20, "name": "Oralia Fulgham" },
];
