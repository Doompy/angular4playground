import { Component, OnInit } from '@angular/core';

import { Customer } from '../../lib/types';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'dashboard-page',
  templateUrl: './dashboard.html',
  styles: [
    `
      .tile {
        display: flex;
        flex-flow: row;
        justify-content: center;
        align-items: center;
        height: 100px;
        background-color: #EFEFEF;
        border: 1px solid #CCCCCC;
      }
      .tile:hover {
        cursor: pointer;
        background-color: #DEDEFF;
      }
    `
  ]
})
export class DashboardPage implements OnInit {
  customers: Customer[] = [];

  constructor(private customerService: CustomerService) {

  }

  ngOnInit(): void {
    this.customerService.getCustomers()
      .then(customers => this.customers = customers.slice(1, 5));
  }
}
