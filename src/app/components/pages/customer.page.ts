import { Component } from '@angular/core';

import { CustomersComponent } from '../customers/customers.component';

@Component({
  selector: 'customer-page',
  template: `
    <article class="container">
      <customers></customers>
    </article>
  `
})
export class CustomerPage {

}
