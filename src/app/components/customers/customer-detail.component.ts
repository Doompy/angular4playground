import { Component, Input } from '@angular/core';

import { Customer } from '../../lib/types';

@Component({
  selector: 'customer-detail',
  templateUrl: './customer-detail.html'
})
export class CustomerDetailComponent {
  @Input() customer: Customer;
}
