import { Component, OnInit } from '@angular/core';

import { Customer } from '../../lib/types';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'customers',
  styleUrls: [ './customers.css' ],
  templateUrl: './customers.html',
})
export class CustomersComponent implements OnInit {
  customers: Customer[];
  selectedCustomer: Customer;

  // inject the service into local props
  constructor(private customerService: CustomerService) {};

  // ng lifecycle method
  ngOnInit(): void {
    this.getCustomers();
  };

  getCustomers(): void {
    this.customerService.getCustomers().then(customers => this.customers = customers);
  }

  onSelect(customer: Customer): void {
    this.selectedCustomer = customer;
  }
}
