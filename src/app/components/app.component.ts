import { Component } from '@angular/core';

@Component({
  selector: 'app',
  templateUrl: './app.html',
  styles: [
    `
      nav {
       margin: 20px 0;
      }
      nav a {
        padding: 14px;
        color: black;
        background-color: #EFEFEF;
        border: 1px solid #CCCCCC;
      }
      nav a:hover {
        cursor: pointer;
        color: black;
        text-decoration: none;
        background-color: #DEDEFF;
      }
    `
  ]
})
export class AppComponent {
  title = 'Angular 4 Playground';
}
