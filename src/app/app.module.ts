import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

// routes
import Routes from './app.routes';
// page declarations
import { CustomerPage } from './components/pages/customer.page';
import { DashboardPage } from './components/pages/dashboard.page';
// component declarations
import { AppComponent } from './components/app.component';
import { CustomersComponent }  from './components/customers/customers.component';
import { CustomerDetailComponent } from  './components/customers/customer-detail.component';
// service declarations
import { CustomerService } from './services/customer.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    Routes
  ],
  declarations: [
    CustomerPage,
    DashboardPage,
    AppComponent,
    CustomersComponent,
    CustomerDetailComponent
  ],
  providers: [
    CustomerService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
