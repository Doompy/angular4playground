import { RouterModule }  from '@angular/router';
import { CustomerPage }  from './components/pages/customer.page';
import { DashboardPage }  from './components/pages/dashboard.page';

export default RouterModule.forRoot([
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'customers',
    component: CustomerPage
  },
  {
    path: 'dashboard',
    component: DashboardPage
  }
])
